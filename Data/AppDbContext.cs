﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models.Data_Models.Personage;

namespace Data
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public DbSet<Personage> Personages { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }
    }
}
