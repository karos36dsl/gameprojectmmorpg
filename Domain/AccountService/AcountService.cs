﻿using Domain.OtherService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Models.Data_Models.Personage;
using Models.Enum;
using Models.Vm_Models.AccountWork;
//using System.Security.Policy;
using System.Threading.Tasks;

namespace Domain.AccountService
{
    public class AcountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly EmailService _emailService;

        public AcountService(UserManager<User> userManager, SignInManager<User> signInManager, EmailService emailService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
        }

        public async Task<object> Register(RegisterViewModel model)
        {
            User user = new User 
            { 
                Email = model.Email, 
                UserName = model.Email, 
                Age = model.Age, 
                FirstName = model.FirstName, 
                LastName = model.LastName 
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                return new { userId = user.Id, code = code };
            }
            else
            {
                return null;
            }
        }

        public async Task<MyOperationResult> SendEmail (string email, string callbackUrl)
        {
            var result = await _emailService.SendEmailAsync(email, 
                "Confirm your account",
                $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");
            if (result == SendMailOperationResult.Send)
            {
                return MyOperationResult.Success;
            }
            else
            {
                return MyOperationResult.UnSuccess;
            }
        }

        public async Task<MyOperationResult> ConfirmUser(string userId, string code)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return MyOperationResult.UnSuccess;
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
                return MyOperationResult.Success;
            else
                return MyOperationResult.UnSuccess;
        }

        public async Task<MyOperationResult> Login(LoginViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user != null)
            {
                // проверяем, подтвержден ли email
                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    return MyOperationResult.NoConfirm;// не подтвержденный email
                }
            }

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
            if (result.Succeeded)
            {
                return MyOperationResult.Success;
            }
            else
            {
                return MyOperationResult.LoginNoFound;//не верный логин или пароль
            }
        }

        public async Task LogOff()
        {
            await _signInManager.SignOutAsync();
        }
    }
}
