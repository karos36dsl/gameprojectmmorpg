﻿using MailKit.Net.Smtp;
using MimeKit;
using Models.Enum;
using System;
using System.Threading.Tasks;

namespace Domain.OtherService
{
    public class EmailService
    {
        public async Task<SendMailOperationResult> SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "mmorpg36dsl@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            try
            {
                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.gmail.com", 465, true);
                    await client.AuthenticateAsync("mmorpg36dsl@gmail.com", "Mmorpg36");
                    await client.SendAsync(emailMessage);
                    await client.DisconnectAsync(true);
                }
                return SendMailOperationResult.Send;
            }
            catch(Exception ex)
            {
                return SendMailOperationResult.NoSend;
            }
           
        }
    }
}
