﻿
using System.Threading.Tasks;
using Domain.AccountService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Enum;
using Models.Vm_Models.AccountWork;

namespace GameProjectMmo.Controllers
{
    public class AccountController : Controller
    {
        private readonly AcountService _acountService ;

        public AccountController(AcountService acountService)
        {
            _acountService = acountService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _acountService.Register(model);
                if(result != null)
                {
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        result,
                        protocol: HttpContext.Request.Scheme);
                    var resultOperation = await _acountService.SendEmail(model.Email, callbackUrl);
                    if (resultOperation == MyOperationResult.Success)
                    {
                        return Content("Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");
                    }
                    else
                    {
                        return Content("При отправке письма возникла ошибка");
                    }
                }
                return Content("Ошибка при регистрации пользователя");
            }
            return Content("Не верные входные данные");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return Content("Error");
            }
            var confirmResult = await _acountService.ConfirmUser(userId, code);
            if(confirmResult == MyOperationResult.Success)
            {
                return Content("Почта Подтверждена");
            }
            else
            {
                return Content("Почта Не Подтверждена");
            }
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var operationLoginResult = await _acountService.Login(model);
                switch (operationLoginResult)
                {
                    case MyOperationResult.Success:
                        return RedirectToAction("Index", "Home");
                    case MyOperationResult.UnSuccess:
                        return Content("Что то пошло не так");
                    case MyOperationResult.NoConfirm:
                        return Content("Почта не подтверждена");
                    case MyOperationResult.LoginNoFound:
                        return Content("Не верный логин или пароль"); 
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _acountService.LogOff();
            return Content("Успешно вышел из системы");
        }
    }
}
