﻿
using Microsoft.AspNetCore.Identity;

namespace Models.Data_Models.Personage
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

    }
}
